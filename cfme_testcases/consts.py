# -*- coding: utf-8 -*-
"""
Constants.
"""

from __future__ import unicode_literals

TEST_SUITE_XML = "test_suite_import.xml"
TEST_CASE_XML = "test_case_import.xml"
REQUIREMENTS_XML = "test_requirements_import.xml"
TESTS_DATA_JSON = "tests_data.json"
