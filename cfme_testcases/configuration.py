# -*- coding: utf-8 -*-
"""
Configuration loading.
"""

from __future__ import absolute_import, unicode_literals

# pylint: disable=unused-import
from dump2polarion.configuration import get_config  # noqa
